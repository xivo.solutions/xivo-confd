from test_api.setup import setup_sysconfd, setup_provd

from .wrappers import IsolatedAction


class sysconfd(IsolatedAction):
    actions = {'generate': setup_sysconfd}


class provd(IsolatedAction):
    actions = {'generate': setup_provd}
