# -*- coding: utf-8 -*-

# Copyright (C) 2020 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from . import agent
from . import call_permission
from . import cti_profile
from . import device
from . import endpoint_custom
from . import endpoint_sccp
from . import endpoint_sip
from . import entity
from . import extension
from . import funckey_template
from . import label
from . import line
from . import line_device
from . import line_endpoint_custom
from . import line_endpoint_sccp
from . import line_endpoint_sip
from . import line_extension
from . import line_sip
from . import switchboard
from . import user
from . import user_call_permission
from . import user_cti_profile
from . import user_entity
from . import user_funckey_template
from . import user_import
from . import user_line
from . import user_voicemail
from . import voicemail
