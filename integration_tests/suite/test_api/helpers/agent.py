# -*- coding: UTF-8 -*-

# Copyright (C) 2018 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from test_api import config
from test_api import db


def add_agent(agent_number, agent_context, numgroup):
    with db.queries() as queries:
        id = queries.insert_agent(number=agent_number, context=agent_context, numgroup=numgroup)
    return {'id': id,
            'number': agent_number,
            'numgroup': numgroup}


def generate_agent(number, context=config.CONTEXT, numgroup=1):
    return add_agent(number, context, numgroup)


def delete_agent(agent_id, check=False):
    with db.queries() as queries:
        queries.delete_agent(agent_id)
