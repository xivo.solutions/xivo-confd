# -*- coding: utf-8 -*-

# Copyright (C) 2024 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os

from test_api.setup import setup_provd, setup_database
from xivo_test_helpers.asset_launching_test_case import AssetLaunchingTestCase


class BaseIntegrationTest(AssetLaunchingTestCase):
    assets_root = os.path.join(os.path.dirname(__file__), '..', '..', 'assets')
    asset = 'base'
    service = 'confd'
    cur_dir = os.getcwd()


class BaseIntegrationTestStart(BaseIntegrationTest):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        setup_provd()
        setup_database()
    
    @classmethod
    def tearDownClass(cls):
        pass

class BaseIntegrationTestStop(BaseIntegrationTest):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

