from . import BaseIntegrationTestStart
from hamcrest import assert_that, equal_to

# Dummy test class used to start test asset before all test
#
# This class must in a file named in order to be fetched *first*
#
# By default the unittest discover method discovers test in file
# with pattern test*.py
# See https://docs.python.org/3/library/unittest.html#unittest.TestLoader.discover

class StartAsset(BaseIntegrationTestStart):

    def test_aaa_empty_test_for_setup(self):
        assert_that("start", equal_to("start"))