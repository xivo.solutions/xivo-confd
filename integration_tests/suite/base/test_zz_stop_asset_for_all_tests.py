from . import BaseIntegrationTestStop
from hamcrest import assert_that, equal_to

# Dummy test class used to stop test asset after all test
#
# This class must in a file named in order to be fetched *last*
#
# By default the unittest discover method discovers test in file
# with pattern test*.py
# See https://docs.python.org/3/library/unittest.html#unittest.TestLoader.discover

class StopAsset(BaseIntegrationTestStop):

    def test_zzz_empty_test_for_setup(self):
        assert_that("stop", equal_to("stop"))