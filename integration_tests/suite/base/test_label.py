from hamcrest import assert_that, contains_inanyorder, empty
from hamcrest import equal_to
from hamcrest import has_entries
from test_api import confd
from test_api import fixtures
import unittest

class TestLabel(unittest.TestCase):

    @fixtures.label()
    @fixtures.label()
    def test_list_labels(self, first, second):
        response = confd.labels.get()

        assert_that(response.items, contains_inanyorder(has_entries(first), has_entries(second)))
        assert_that(response.total, equal_to(2))


    @fixtures.label()
    def test_delete_label(self, label):
        deleted = confd.labels(label['id']).delete()
        deleted.assert_deleted()

        label_list = confd.labels.get()
        assert_that(label_list.items, empty())


    def test_add_labels(self):
        response = confd.labels.post(display_name='MyLabel')
        label_list = confd.labels.get()

        assert_that(response.item['display_name'], equal_to('MyLabel'))
        assert_that(label_list.total, equal_to(1))

        confd.labels(response.item['id']).delete()


    @fixtures.label()
    def test_edit_label(self, label):
        form = {'display_name': 'new label name',
                'description': 'new description'}

        response = confd.labels(label['id']).put(**form)

        response.assert_updated()
        assert_that(confd.labels(label['id']).get().item, has_entries(form))
