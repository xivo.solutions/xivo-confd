# XiVO confd

XiVO CONFD is a web server that provides a [RESTful](http://en.wikipedia.org/wiki/Representational_state_transfer)
service for configuring and managing a XiVO server.

## Installing xivo-confd

The server is already provided as a part of [XiVO](http://documentation.xivo.solutions). Please refer
to [the documentation](https://documentation.xivo.solutions/en/stable/xivo/installation/installation.html) for further
details on installing one.

## Running unit tests

1. Run the latest version of the debian-builder container mounting the project directories (HOME by default)

```
docker run --rm -it --user "`id -u`:`id -g`" \
 -v /etc/localtime:/etc/localtime:ro -v /etc/timezone:/etc/timezone:ro \
 -v /etc/group:/etc/group:ro -v /etc/passwd:/etc/passwd:rw \
 -v /etc/shadow:/etc/shadow:ro -v "$HOME":"$HOME" \
 -w "`pwd`" -e HOME=$HOME xivoxc/debian-builder:2021.13.latestdev
```

2. Launch all of them with tox :

```
tox --recreate -e py37
```

## Running integration tests

Below the new procedure to run integration tests :

1. go to the integration_tests folder
   ```
   cd integration_tests
   ```
2. If your dev is linked to a branch in provd, db or rabbitmq, export the branch in an env variable (by default it will
   default to master) respectively:
    - DB_TEST_BRANCH
    - PROVD_TEST_BRANCH
    - RABBITMQ_TEST_BRANCH
    - CONFIG_TEST_BRANCH
    - for example if you need branch *4436_user_advanced_search* for db:
   ```
   export DB_TEST_BRANCH=4436_user_advanced_search
   ```
3. Then launch the test setup (it will build all the containers):
   ```
   make test-setup
   ```
4. Then you can run *all the tests* **or** only *some tests*
    * Run all the tests:
      ```
      make test
      ```
    * **or** Run only one suite (you must be inside the container):
        1. run an instance of the debian-builder container, mounting the docker socket, the network and the project
           directories (HOME by default)
           ```
           make run-docker-builder
           ```
        2. (**Inside the container**) create and activate the venv:
           ```
            python3 -m venv /tmp/venv3; \
            source /tmp/venv3/bin/activate;
           ```
        3. (**Inside the container**) Install tests requirements:
           ```
            cd .. ; python setup.py egg_info ; cd - ;\
            pip install pip==20.3.4; \
            pip install -r test-requirements.txt;
           ```
        4. (**Inside the container**) Then you can run all the tests:
            * Run all the tests:
              ```
              INTEGRATION_TEST_TIMEOUT=240 python3 -m unittest discover suite -cv
              ```
5. Then you can clean the test environment:
1. **exit** from the container:
   ```
   exit
   ```
2. and run the clean command:
   ```
   make distclean
   ```

### Note about integration test

- When migrating from nosetest to unittest we faced several issues.
- The main one was that unittest does not have a before all and after all setup/teardown method (it exists per class, per module (i.e. file) but not for the whole suite)
- Currently, to reproduce the "old way" we tricked unittest with
  - a file `test_aa_start_asset_for_all_tests` : 
    - as it is named `test_aa_` we "ensure" it will be discovered first
    - and we have a dummy test case which subclasses the BaseIntegrationTestStart method which will start the asset
  - a file `test_zz_stop_asset_for_all_tests` :
    - as it is named `test_zz_` we "ensure" it will be discovered last
    - and we have a dummy test case which subclasses the BaseIntegrationTestStop method which will stop the asset

**Problem** currently this does not permit to launch only one test.
To launch only one test you must:
- launch the asset manually: 
  - cd integration_tests/assets/base
  - docker compose run --rm sync
- then launch the test (via the docker): 
  - cd integration_tests/suite/base
  - python3 -m unittest -fcv base.test_call_logs.TestCallLogs

## Development

### xivo-dao

In case you need to mount xivo_dao inside the xivo-confd container, add the following line in confd volumes in
integration_tests/assets/base/docker-compose.yml

```
- "/path/to/xivo_dao:/usr/local/lib/python2.7/site-packages/xivo_dao"
```

### Modified database

If you need to run tests against a modified database schema, run:

```
make update-db DB_DIR=../../xivo-db DB_VERSION=2019.12.latest
```
