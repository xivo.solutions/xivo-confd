# -*- coding: utf-8 -*-

# Copyright (C) 2020 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from xivo_dao.resources.labels import dao as labels_dao
from xivo_dao.resources.labels import validator as dao_labels_validator

from xivo_confd.helpers.resource import CRUDService
from xivo_confd.plugins.label.notifier import build_notifier
from xivo_confd.plugins.label.validator import build_validator


class LabelService(CRUDService):

    def __init__(self, dao, validator, notifier):
        super(LabelService, self).__init__(dao, validator, notifier)


def build_service():
    return LabelService(labels_dao,
                        build_validator(dao_labels_validator),
                        build_notifier())
